// Copyright (C) 2018 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.microservices.location.controller;

import java.time.Instant;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import de.ixilon.microservices.location.spi.GeocodingProvider;
import de.ixilon.microservices.location.spi.GeocodingProviderException;
import de.ixilon.microservices.location.spi.Location;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * This controller defines the REST API endpoints.
 */
@RestController
public class LocationController {

  private static final String ETAG = System.getenv("CI_BUILD_REF");
  private static final long LAST_MODIFIED = Instant.now().toEpochMilli();
  private static final String CACHE_CONTROL_HEADER =
      CacheControl.maxAge(1, TimeUnit.DAYS).cachePublic().mustRevalidate().getHeaderValue();

  private final GeocodingProvider provider;

  @Autowired
  public LocationController(GeocodingProvider provider) {
    this.provider = provider;
  }

  @ApiOperation(value = "Returns location of a given postal address", response = Location.class)
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "Not Found")})
  @RequestMapping(value = "/location", method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON)
  public ResponseEntity<Location> getLocationByAddress(@RequestParam String address,
      @RequestParam(defaultValue = "0", required = false) double longitude,
      @RequestParam(defaultValue = "0", required = false) double latitude, WebRequest webRequest) {
    if (webRequest.checkNotModified(ETAG, LAST_MODIFIED)) {
      return null;
    }
    Location currentLocation =
        Location.builder().withLongitude(longitude).withLatitude(latitude).build();
    try {
      Location result = provider.getLocationByAddress(address, currentLocation);
      return ResponseEntity.ok().header(HttpHeaders.CACHE_CONTROL, CACHE_CONTROL_HEADER)
          .body(result);
    } catch (GeocodingProviderException exception) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @ApiOperation(value = "Returns postal address of a given location", response = Location.class)
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "Not Found")})
  @RequestMapping(value = "/address", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN)
  public ResponseEntity<String> getAddressByLocation(@RequestParam double longitude,
      @RequestParam double latitude, WebRequest webRequest) {
    if (webRequest.checkNotModified(ETAG, LAST_MODIFIED)) {
      return null;
    }
    Location location = Location.builder().withLongitude(longitude).withLatitude(latitude).build();
    try {
      String result = provider.getAddressByLocation(location);
      return ResponseEntity.ok().header(HttpHeaders.CACHE_CONTROL, CACHE_CONTROL_HEADER)
          .body(result);
    } catch (GeocodingProviderException exception) {
      return new ResponseEntity<>(exception.getMessage(), HttpStatus.NOT_FOUND);
    }
  }

}
