// Copyright (C) 2018 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.microservices.location.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.context.request.WebRequest;

import de.ixilon.microservices.location.LocationArgumentMatcher;
import de.ixilon.microservices.location.spi.GeocodingProvider;
import de.ixilon.microservices.location.spi.GeocodingProviderException;
import de.ixilon.microservices.location.spi.Location;

@RunWith(MockitoJUnitRunner.class)
public class LocationControllerTest {

  @Mock
  private GeocodingProvider providerMock;

  @Mock
  private WebRequest webRequest;

  private LocationController controller;

  @Before
  public void setUp() {
    controller = new LocationController(providerMock);
  }

  @Test
  public void getsLocationByAddress() throws GeocodingProviderException {
    Location expected = Location.builder().withLatitude(1).withLongitude(2).build();

    when(providerMock.getLocationByAddress(eq("foo"), any(Location.class))).thenReturn(expected);

    Location actual = controller.getLocationByAddress("foo", 0, 0, webRequest).getBody();

    assertEquals(expected.getLatitude(), actual.getLatitude(), Double.MIN_VALUE);
    assertEquals(expected.getLongitude(), actual.getLongitude(), Double.MIN_VALUE);
  }

  @Test
  public void getsAddressByLocation() throws GeocodingProviderException {
    String expected = "foo";
    Location location = Location.builder().withLatitude(1).withLongitude(2).build();

    when(providerMock.getAddressByLocation(argThat(new LocationArgumentMatcher(location))))
        .thenReturn(expected);

    String actual =
        controller.getAddressByLocation(location.getLongitude(), location.getLatitude(), webRequest)
            .getBody();

    assertEquals(expected, actual);
  }

}
