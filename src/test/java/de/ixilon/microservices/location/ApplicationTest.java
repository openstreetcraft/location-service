// Copyright (C) 2018 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.microservices.location;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import de.ixilon.microservices.location.spi.Location;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ApplicationTest {

  @Autowired
  private TestRestTemplate restTemplate;

  @Test
  public void getsLocationByAddress() {
    Location expected = 
        Location.builder().withLatitude(37.4224082).withLongitude(-122.0856086).build();
    Location actual = restTemplate.getForObject(
        "/location?address=9911+Amphitheatre+Parkway,+Mountain+View,+CA",
        Location.class);

    assertEquals(expected.getLatitude(), actual.getLatitude(), 0.005);
    assertEquals(expected.getLongitude(), actual.getLongitude(), 0.005);
  }

  @Test
  public void getsAddressByLocation() {
    String expected = "41 Amphitheatre Pkwy, Mountain View, CA 94043, USA";
    String actual = restTemplate
        .getForObject("/address?longitude=-122.0856086&latitude=37.4224082", String.class);
    assertEquals(expected, actual);
  }
}
