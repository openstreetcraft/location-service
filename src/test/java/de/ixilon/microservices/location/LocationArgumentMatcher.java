// Copyright (C) 2018 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.microservices.location;

import static org.junit.Assert.assertEquals;

import org.mockito.ArgumentMatcher;

import de.ixilon.microservices.location.spi.Location;

public class LocationArgumentMatcher extends ArgumentMatcher<Location> {

  private final Location expected;

  public LocationArgumentMatcher(Location expected) {
    this.expected = expected;
  }

  @Override
  public boolean matches(Object argument) {
    Location actual = (Location) argument;

    assertEquals(expected.getLatitude(), actual.getLatitude(), Double.MIN_VALUE);
    assertEquals(expected.getLongitude(), actual.getLongitude(), Double.MIN_VALUE);

    return true;
  }

}
