// Copyright (C) 2021 Joshua Pacifici <jpac14@outlook.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.location.provider.maptiler;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.ixilon.microservices.location.spi.GeocodingProviderException;
import de.ixilon.microservices.location.spi.Location;

public class MaptilerGeocodingProviderTest {
  private final MaptilerGeocodingProvider provider = new MaptilerGeocodingProvider();
  private static final double DELTA = 0.005;
  
  @Test
  public void getsLocationByAddress() throws GeocodingProviderException {
    String address = "1600 Amphitheatre Parkway, Mountain View, CA 94043, USA";
    Location expected =
        Location.builder().withLatitude(37.4224082).withLongitude(-122.0856086).build();
    Location actual = provider.getLocationByAddress(address, null);

    assertEquals(expected.getLatitude(), actual.getLatitude(), DELTA);
    assertEquals(expected.getLongitude(), actual.getLongitude(), DELTA);
  }
  
  @Test
  public void getsLocationByAddressWithACurrentLocation() throws GeocodingProviderException {
    String address = "1600 Amphitheatre Parkway, Mountain View, CA 94043, USA";
    Location currentLocation = 
        Location.builder().withLatitude(37).withLongitude(-122).build();
    
    Location expected =
        Location.builder().withLatitude(37.4224082).withLongitude(-122.0856086).build();
    Location actual = provider.getLocationByAddress(address, currentLocation);

    assertEquals(expected.getLatitude(), actual.getLatitude(), DELTA);
    assertEquals(expected.getLongitude(), actual.getLongitude(), DELTA);
  }

  @Test
  public void getsAddressByLocation() throws GeocodingProviderException {
    Location location =
        Location.builder().withLatitude(37.4224082).withLongitude(-122.0856086).build();
    String expected = "Amphitheatre Parkway, Mountain View, Santa Clara County, California";
    String actual = provider.getAddressByLocation(location);

    assertEquals(expected, actual);
  }
}
