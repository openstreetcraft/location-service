// Copyright (C) 2021 Joshua Pacifici <jpac14@outlook.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.location.provider.maptiler;

import java.util.List;
import java.util.Objects;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.common.collect.ImmutableMap;

import de.ixilon.microservices.location.spi.GeocodingProvider;
import de.ixilon.microservices.location.spi.GeocodingProviderException;
import de.ixilon.microservices.location.spi.Location;
import mil.nga.sf.geojson.FeatureCollection;
import mil.nga.sf.geojson.FeatureConverter;

public class MaptilerGeocodingProvider implements GeocodingProvider {
  
  private static final String API_KEY = Objects.requireNonNull(
      System.getenv("MAPTILER_CLOUD_API_KEY"), "Missing MAPTILER_CLOUD_API_KEY");
    
  private static final UriComponentsBuilder BASE_URI_BUILDER = 
      UriComponentsBuilder.newInstance().scheme("https")
      .host("api.maptiler.com")
      .query("key={key}");
  
  private final RestTemplate restTemplate = new RestTemplateBuilder().build();
  
  @Override
  public Location getLocationByAddress(String address, Location currentLocation)
      throws GeocodingProviderException {
    String content = restTemplate.getForObject(
        constructForwardRequestUri(address, currentLocation), String.class);
    
    FeatureCollection features = FeatureConverter.toFeatureCollection(content);
    
    @SuppressWarnings("unchecked")
    List<Double> center = (List<Double>) features.getFeature(0).getForeignMember("center");
    
    return Location.builder().withLongitude(center.get(0)).withLatitude(center.get(1)).build();
  }

  @Override
  public String getAddressByLocation(Location location) throws GeocodingProviderException {
    String content = restTemplate.getForObject(
        constructReverseRequestUri(location), String.class);
    
    FeatureCollection features = FeatureConverter.toFeatureCollection(content);
            
    return (String) features.getFeature(0).getForeignMember("place_name");
  }
  
  private String constructForwardRequestUri(String address, Location currentLocation) {
    if (currentLocation == null) {      
      return BASE_URI_BUILDER.cloneBuilder().path("geocoding/{address}.json")
          .buildAndExpand(ImmutableMap.of("address", address, "key", API_KEY)).toString();
    }
    
    return BASE_URI_BUILDER.cloneBuilder().path("geocoding/{address}.json")
        .query("proximity={lng},{lat}")
        .buildAndExpand(ImmutableMap.of(
            "address", address,
            "lng", currentLocation.getLongitude(),
            "lat", currentLocation.getLatitude(),
            "key", API_KEY))
        .toString();
  }
  
  private String constructReverseRequestUri(Location location) {
    return BASE_URI_BUILDER.cloneBuilder().path("geocoding/{lng},{lat}.json")
        .buildAndExpand(ImmutableMap.of(
            "lng", location.getLongitude(),
            "lat", location.getLatitude(),
            "key", API_KEY))
        .toString();
  }
}
