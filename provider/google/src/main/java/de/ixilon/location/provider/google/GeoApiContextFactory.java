// Copyright (C) 2018 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.location.provider.google;

import com.google.maps.GeoApiContext;

class GeoApiContextFactory {

  public GeoApiContext createGeoApiContext() {
    GeoApiContext.Builder builder = new GeoApiContext.Builder();

    String key = System.getenv("GOOGLE_MAPS_GEOCODING_API_KEY");

    if (key != null) {
      return builder.apiKey(key).build();
    }

    String id = System.getenv("GOOGLE_MAPS_GEOCODING_API_CLIENT");
    String secret = System.getenv("GOOGLE_MAPS_GEOCODING_API_SECRET");

    if ((id != null) && (secret != null)) {
      return builder.enterpriseCredentials(id, secret).build();
    }

    throw new IllegalStateException("Missing GOOGLE_MAPS_GEOCODING_API_KEY");
  }

}
