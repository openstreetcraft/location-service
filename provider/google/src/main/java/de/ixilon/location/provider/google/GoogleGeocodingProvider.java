// Copyright (C) 2018 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.location.provider.google;

import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;

import de.ixilon.microservices.location.spi.GeocodingProvider;
import de.ixilon.microservices.location.spi.GeocodingProviderException;
import de.ixilon.microservices.location.spi.Location;

public class GoogleGeocodingProvider implements GeocodingProvider {

  private static final GeoApiContextFactory factory = new GeoApiContextFactory();
  private final GeoApiContext context;

  public GoogleGeocodingProvider() {
    this(factory.createGeoApiContext());
  }

  public GoogleGeocodingProvider(GeoApiContext context) {
    this.context = context;
  }

  @Override
  public Location getLocationByAddress(String address, Location currentLocation)
      throws GeocodingProviderException {
    final GeocodingResult[] results;

    try {
      results = GeocodingApi.geocode(context, address).await();
    } catch (ApiException | InterruptedException | IOException exception) {
      throw new GeocodingProviderException(exception);
    }
    
    Comparator<GeocodingResult> byDistance = new Comparator<GeocodingResult>() {
      @Override
      public int compare(GeocodingResult lhsResult, GeocodingResult rhsResult) {
        double lhsDistance = toLocation(lhsResult).distance(currentLocation);
        double rhsDistance = toLocation(rhsResult).distance(currentLocation);
        return Double.compare(lhsDistance, rhsDistance);
      }
    };

    if (currentLocation != null) {
      Arrays.sort(results, byDistance);
    }

    try {
      return toLocation(results[0]);
    } catch (ArrayIndexOutOfBoundsException exception) {
      throw new GeocodingProviderException(exception);
    }
  }

  private static Location toLocation(GeocodingResult result) {
    LatLng location = result.geometry.location;
    return Location.builder().withLatitude(location.lat).withLongitude(location.lng).build();
  }

  @Override
  public String getAddressByLocation(Location location) throws GeocodingProviderException {
    final GeocodingResult[] results;

    try {
      results = GeocodingApi.reverseGeocode(context, toLatLng(location)).await();
    } catch (ApiException | InterruptedException | IOException exception) {
      throw new GeocodingProviderException(exception);
    }

    try {
      return results[0].formattedAddress;
    } catch (ArrayIndexOutOfBoundsException exception) {
      throw new GeocodingProviderException(exception);
    }
  }

  private static LatLng toLatLng(Location location) {
    return new LatLng(location.getLatitude(), location.getLongitude());
  }

}
