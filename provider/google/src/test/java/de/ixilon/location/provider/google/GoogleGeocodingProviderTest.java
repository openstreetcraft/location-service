// Copyright (C) 2018 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.location.provider.google;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.ixilon.microservices.location.spi.GeocodingProvider;
import de.ixilon.microservices.location.spi.GeocodingProviderException;
import de.ixilon.microservices.location.spi.Location;

public class GoogleGeocodingProviderTest {

  private GeocodingProvider provider = new GoogleGeocodingProvider();

  @Test
  public void getsLocationByAddress() throws GeocodingProviderException {
    String address = "1600 Amphitheatre Parkway, Mountain View, CA 94043, USA";
    Location expected =
        Location.builder().withLatitude(37.4224639).withLongitude(-122.0841909).build();
    Location actual = provider.getLocationByAddress(address, null);

    assertEquals(expected.getLatitude(), actual.getLatitude(), 0.005);
    assertEquals(expected.getLongitude(), actual.getLongitude(), 0.005);
  }

  @Test
  public void getsAddressByLocation() throws GeocodingProviderException {
    Location location =
        Location.builder().withLatitude(37.4224082).withLongitude(-122.0856086).build();
    String expected = "41 Amphitheatre Pkwy, Mountain View, CA 94043, USA";
    String actual = provider.getAddressByLocation(location);

    assertEquals(expected, actual);
  }

}
