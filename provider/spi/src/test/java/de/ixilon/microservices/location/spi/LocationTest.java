// Copyright (C) 2020 Joshua Pacifici <jpac14@outlook.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.microservices.location.spi;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class LocationTest {
  @Test
  public void calculatesTheDistanceToAnotherLocation() {
    Location currentLocation =
        Location.builder().withLatitude(1.2463).withLongitude(8.38485).build();
    Location otherLocation = Location.builder().withLatitude(7.6583).withLongitude(3.2858).build();

    double excepted = 8.192317;
    double actual = currentLocation.distance(otherLocation);

    assertEquals(excepted, actual, 0.005);
  }
}
