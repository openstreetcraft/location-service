// Copyright (C) 2018 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.microservices.location.spi;

public class Location {
  private double latitude;
  private double longitude;

  public double getLatitude() {
    return latitude;
  }

  protected void setLatitude(double latitude) {
    this.latitude = latitude;
  }

  public double getLongitude() {
    return longitude;
  }

  protected void setLongitude(double longitude) {
    this.longitude = longitude;
  }

  /**
   * Calculates the distance to another location.
   */
  public double distance(Location other) {
    double opposite = (this.getLatitude() - other.getLatitude());
    double adjacent = (this.getLongitude() - other.getLongitude());

    return Math.sqrt(Math.pow(opposite, 2) + Math.pow(adjacent, 2));
  }

  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {
    private double latitude;
    private double longitude;

    /**
     * Create a Location instance.
     */
    public Location build() {
      Location location = new Location();
      location.setLatitude(latitude);
      location.setLongitude(longitude);
      return location;
    }

    public Builder withLatitude(double latitude) {
      this.latitude = latitude;
      return this;
    }

    public Builder withLongitude(double longitude) {
      this.longitude = longitude;
      return this;
    }
  }

}
