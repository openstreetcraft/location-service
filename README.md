[![Codacy Badge](https://app.codacy.com/project/badge/Grade/1e0cd754dc5e4629a8ad342978c8e69b)](https://www.codacy.com/gl/openstreetcraft/location-service/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=openstreetcraft/location-service&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://app.codacy.com/project/badge/Coverage/1e0cd754dc5e4629a8ad342978c8e69b)](https://openstreetcraft.gitlab.io/location-service/reports/jacoco/jacocoFullReport/html/)
[![Javadoc Badge](https://img.shields.io/badge/javadoc-brightgreen.svg)](https://openstreetcraft.gitlab.io/location-service/docs/javadoc/)
# Overview

Transforms a postal address description to a location on the Earth's surface

The following informations are supported:

* address at location
* location of address

# Software Design

* RESTful webservice aggregating information from different third-party providers
* Provides a common interface to different services (facade pattern)
* Balances load, respects availability and service limits to be more resilient 

# Providers
 
The following providers are currently supported:

* [Google Maps Geocoding API](https://developers.google.com/maps/documentation/geocoding/)

More providers can be integrated as plugins by using a [service provider interface](https://en.wikipedia.org/wiki/Service_provider_interface) (SPI).

# Configuration

Some provider specific environment variables must be set to configure API keys:

* GOOGLE_MAPS_GEOCODING_API_KEY

A registration may be necessary to get an API key:

* [Get Google Maps API Key](https://developers.google.com/maps/documentation/geocoding/get-api-key)

# Runtime

Service can be started with

```
./gradlew distDocker
docker-compose up --build location
```

# REST API

REST API of a running service can be discovered with Swagger UI: 

http://localhost:8080/api/v1/swagger-ui.html

# Links

The project [openstreetcraft-api](https://gitlab.com/openstreetcraft/openstreetcraft-api)
is a Java library for easy access to this service.
